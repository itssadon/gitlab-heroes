# GitLab Heroes

At GitLab, we believe everyone can contribute. In that spirit, we believe everyone in our community can become a GitLab Hero. Whether you are [organizing meetups](https://about.gitlab.com/community/meetups), recording demos for YouTube, giving talks at conferences and events, writing technical blog posts, or [contributing to our open source project](https://about.gitlab.com/community/contribute), we want to engage, support, and recognize you for your contribution.

# Governance

This program is currently managed by GitLab's Evangelist Program Manager. A steering committee has been formed to determine whether and how we can establish a Heroes leadership team that owns goal setting, strategy, recruiting new members, reviewing applications, coaching and supporting new Heroes, and other program-related responsibilities.

The steering commitee is composed of current Heroes, including: 
* [Abubukar Hassan](https://gitlab.com/itssadon)
* [Adrian Moisey](https://gitlab.com/adrianmoisey)
* [Marvin Karegyeya](https://gitlab.com/nuwe1)
* [Nico Meisenzahl](https://gitlab.com/nico-meisenzahl)
* [Vladimir Roudakov](https://gitlab.com/VladimirAus)
* [James Tharpre (observer)](https://gitlab.com/jamestharpe-tmo)

## Why become a GitLab Hero

GitLab Hero is designed to help our community members grow and develop. We want to invest in the professional development of Heroes, help them develop new skills like community organizing and public speaking, and turn them into advocates for best practices in the technology community so that they can help others grow and develop in their understanding of open source, DevOps, and software development.  

## Hero Rewards
* Invitations to special events including GitLab Commit
* Support for travel to speak about GitLab at events
* GitLab Gold and Ultimate licenses
* Special Heroes swag so people know how awesome you are
* Access to GitLab's product, engineering, and technical evangelism teams to help with reviews of talks and blog posts

## Attributes of a Hero
* Possess a passion for GitLab, DevOps, and Open Source software.
* Aspire to our mission, uphold our values, and adhere to our Code of Conduct.
* Sustained level of contribution to GitLab as a code contributor, community organizer, or thought leader.

## Levels 
The Heroes program has three levels (Contributor, Hero, and Superhero) with corresponding contributions and rewards which can be found on our [GitLab Heroes](https://about.gitlab.com/community/heroes/#heros-journey) page. 

## Nominations
Nominate yourself or another member of the community by visiting the [GitLab Heroes](https://about.gitlab.com/community/heroes/#apply) page.

## Contributions 

Heroes are encouraged to create issues when working on contributions to the GitLab community. Using issues help us to better collaborate with our Heroes and provides greater transparency to the GitLab community. 

The following templates can be used to share your contributions: 

* When working on a CFP submission, tech talk, blog post, video, or other content, please use the [Heroes Content](https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-heroes/issues/new?issuable_template=heroes-content) issue template.
* When organizing a meetup, please use the [Meetup Organizer](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues/new?issuable_template=meetup-organizer) issue template. 
* When contributing to GitLab, visit our [Contribute to GitLab Page](https://about.gitlab.com/community/contribute/) for more info. 

## Communication 

Please communicate using MRs and Issues in the GitLab Heroes project whenever possible to provide transparency to the wider GitLab community and allow for easier collaboration. If you need to reach the Evangelist Program Manager from GitLab, please email evangelists@gitlab.com. 

There is a [GitLab Heroes room](https://gitter.im/gitlabhq/heroes#) in the GitLab Gitter community that is used to chat about upcoming events, Meetups, GitLab Heroes, and other non-code topics.  

## Questions / Feedback
If anyone has any thoughts, concerns, ideas for how we can improve the program, please open an Issue here or create a Merge Requests (to this project, the GitLab Heroes page, and the GitLab Handbook)! Thanks!  
